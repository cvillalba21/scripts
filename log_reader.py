#!/usr/bin/env python
"""
Utility for reading iarc7 simulator logs
It can do differents actions:
   Write a resume file with the most relevant information
   Get the number of executions with same log file
Example: python log_reader.py log /home/user/logs/exec1/
         python log_reader.py length /home/path/exec2/
@author Carlos Villalba Coronado
@date 11/09/2014
@version 1.1
"""
import time
import datetime
import re
import sys


class Duration(datetime.timedelta):
    def __str__(self):
        return str(self.seconds / 60) + ":" + str(self.seconds % 60)


class SingleSimRecord:
    def __init__(self, robots_in=0, robots_out=0, time=Duration(minutes=10.0)):
        self.robots_in = robots_in
        self.robots_out = robots_out
        self.time = time

    def __str__(self):
        return "Robots IN: " + str(self.robots_in) + ", Robots OUT: " + \
               str(self.robots_out) + ". In " + str(self.time) + " minutes"


class SimulationsResults:
    def __init__(self, log_path):
        self.log_path = log_path
        self.results = []
        #read the results for the log file
        f = open(self.log_path + 'Scene_simulator.log', 'r')
        for l in f.readlines():
            if re.search("[0-9] robots IN, [0-9] robots OUT", l):
                if re.search("SUCCEDED", l):
                    nums = re.search("SUCCEDED: ([0-9]+) robots IN, ([0-9]+) robots OUT at ([0-9]+):([0-9]+)", l)
                    mins = float(nums.group(3))
                    secs = float(nums.group(4))
                    self.results.append(SingleSimRecord(int(nums.group(1)),
                        int(nums.group(2)),
                        Duration(minutes=mins, seconds=secs)))
                else:
                    nums = re.search("ED:(.+?) robots IN, (.+?) robots OUT", l)
                    self.results.append(SingleSimRecord(int(nums.group(1)),
                         int(nums.group(2)), Duration(minutes=10.0)))
        f.close()

    def average(self):
        robots_in = 0.0
        robots_out = 0.0
        time = Duration(seconds=0.0)
        i = 0.0
        for r in self.results:
            i += 1.0
            robots_in += float(r.robots_in)
            robots_out += float(r.robots_out)
            time += r.time
        return SingleSimRecord(robots_in / i, robots_out / i, time / int(i))

    def completed(self):
        s = 0
        for r in self.results:
            if r.time.seconds < 10.0 * 60:
                s += 1
        return s

    def write_log(self):
        f = open(self.log_path + 'info.log', 'a')
        f.write("------------------------------\n")
        f.write("Averages of " + str(len(self.results)) + " executions:\n")
        f.write(str(self.average()) + "\n")
        f.write("------------------------------\n")
        f.write(str(self.completed()) +
            " veces que se ha terminado con exito\n")
        f.write("------------------------------\n")
        i = 1
        for r in self.results:
            f.write("Execution " + str(i) + " - " + str(r) + "\n")
            i += 1
        f.close()


def log(path):
    sim_results = SimulationsResults(path)
    sim_results.write_log()


def _length(path):
    sim_results = SimulationsResults(path)
    print len(sim_results.results)

if len(sys.argv) >= 3:
    action = sys.argv[1]
    path = sys.argv[2]
    if(action == 'log'):
        log(path)
    elif(action == 'length'):
        _length(path)
    else:
        print "I need and action in the first param follow for its params"
        print "    Example 1: python log_reader.py log /home/user/logs/exec1/"
        print "    Example 2: python log_reader.py length /home/path/exec2/"

else:
    print "I need and action in the first param follow for its params"
    print "    Example 1: python log_reader.py log /home/user/logs/exec1/"
    print "    Example 2: python log_reader.py length /home/path/exec2/"
