#!/bin/bash

startroscore(){
	echo "Starting roscore..."
	roscore &> /dev/null &
	sleep 2
}

startparameterserver(){
	echo "Start parameter server..."
	rosparam load parameters.yml
	sleep 1
}

startrviz(){
	echo "Starting rviz..."
	rosrun rviz rviz &> /dev/null &
	sleep 6	
}

startnodes(){
	source ~/iarc7_ws/devel/setup.bash #CHANGE TO LOCAL DIRECTORY
	echo "Starting simulation ROS nodes..."
	rosrun scene_drawer scene_drawer.py &
	sleep 1
	rosrun perception_system perception_system.py &
	sleep 1
	rosrun flight_controller flight_controller.py &
	sleep 1
	rosrun mission_planner mission_planner.py &
	sleep 1
	rosrun scene_simulator scene_simulator.py &
}

stoproscore(){

	echo "Stopping roscore..."
	killall roscore
}

stoprviz(){

	if [ -n "`pidof rviz`" ]; then
		echo "Stopping rviz..."
		killall rviz
	else
		echo "No process rviz to kill"
	fi
}

stopnodes(){
	echo "Stopping nodes..."
	killall python
}

case "$1" in

	start)
			startroscore
			startparameterserver
			startrviz
			startnodes
			;;

	stop)

			stoproscore
			stoprviz
			stopnodes
			;;

	restartall)
			echo "Restarting simulation..."
			stopnodes
			sleep 1
			stoprviz
			sleep 1
			stoproscore
			sleep 1
			start
			;;

	restart)
			echo "Restarting simulation ROS nodes..."
			stopnodes
			sleep 2
			startnodes
			;;

	stopnodes)
			stopnodes
			;;

	stoprviz)

			stoprviz
			;;

	stoproscore)

			stoproscore
			;;
esac

exit 0
