#!/bin/bash

#Script for launch executions of the simulator in a row
#It has two parameters: first is a description with the relevant information, 
#                       second is optional and is the number of executions
#
#Examples of use: ./iarc_exe.sh "This is a execution with default number of executions"
#                 ./iarc_exe.sh "This is a execution using the optional param" 10

#Simulator with absolute path
SIMULATOR="/home/cvillalba/iarc7_ws/src/./simulator.sh"
#Log reader with absolute path
LOG_READER="/home/cvillalba/scripts/log_reader.py"
#New directory is created corresponding with the date
DATE=`date +%Y-%m-%d___%H-%M-%S`
LOG_PATH="/home/cvillalba/Desktop/log/$DATE/"  

#Number of executions
let EXECS=${2:-4}                                 

export ROS_LOG_DIR=$LOG_PATH

#Executions
$SIMULATOR start
sleep 50
let CURRENT=$(python $LOG_READER length $LOG_PATH)
let NEXT=1    
sleep 12
until [ $CURRENT -ge $EXECS ]; do    #Loop to check if has finished
    if [ $CURRENT -eq $NEXT ];           #If the iter has finished start next 
    then 
        ~/iarc7_ws/src/./simulator.sh restart
        let NEXT+=1
    fi
    let CURRENT=$(python $LOG_READER length $LOG_PATH)
    sleep 12
done
$SIMULATOR stop

#Create an info.log file and append relevant information
echo $1 > "${LOG_PATH}info.log"
python $LOG_READER log $LOG_PATH

